// @flow
import React from 'react';
import {upperCaseFirst} from "../../../../Helper/StringHelper";
import type {CapType} from "../../../../Types/Cap";
import type {KeyType} from "../../../../Types/Key";

type Props = {
  type: CapType | KeyType,
  nameSuffix?: ?string,
  image: string,
  onClick: Function,
  className?: ?string,
  title?: ?string,
  width?: ?number,
};

export default function FlagInput(props: Props) {
  const {type, nameSuffix, image, onClick, className, title, width} = props;
  const realTitle = title !== undefined ? title : `${upperCaseFirst(type)} ${nameSuffix || ''}`;
  const unused = title === 'unused';

  return (
    <span className={'ml-2'}>
      <img
        src={image}
        alt={realTitle}
        title={realTitle}
        className={`cursor-pointer nearest-neighbor ${unused ? 'transparent' : ''} ${className || ''}`}
        width={width || 32}
        onClick={() => onClick(type, nameSuffix)}
      />
    </span>
  );
}
