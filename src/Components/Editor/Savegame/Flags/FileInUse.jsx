// @flow
import React from 'react';
import FlagInput from "./FlagInput";
import iconInUseY from '../../../../Resources/Images/inUseY.png';
import iconInUseN from '../../../../Resources/Images/inUseN.png';
import type {SaveFlagsType} from "../../../../Data/Save";
import {Col, Row} from "react-bootstrap";

type Props = {
  flags: SaveFlagsType,
  updateSaveData: Function,
};

export default function FileInUse(props: Props) {
  const {flags, updateSaveData} = props;

  const onChange = (type: string) => {
    updateSaveData({
      type: 'flag',
      data: {
        flag: type,
        value: !flags.fileInUse,
      }
    });
  }

  const image = flags.fileInUse ? iconInUseY : iconInUseN;
  return (
    <Row className={'row flex-nowrap'}>
      <Col md={4}>
        In&nbsp;use
      </Col>
      <Col md={8}>
        <FlagInput type={'fileInUse'} nameSuffix={''} image={image} onClick={onChange} title={'FileInUse use'} width={48}/>
      </Col>
    </Row>
  );
}
