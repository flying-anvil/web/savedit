// @flow
import React from 'react';
import {Col, Row} from "react-bootstrap";
import iconWingCapActive from '../../../../Resources/Images/Boxes/wing_cap_box_front.rgba16.png';
import iconVanishCapActive from '../../../../Resources/Images/Boxes/vanish_cap_box_front.rgba16.png';
import iconMetalCapActive from '../../../../Resources/Images/Boxes/metal_cap_box_front.rgba16.png';
import iconWingCapInactive from '../../../../Resources/Images/Boxes/WingCapInactive.png';
import iconVanishCapInactive from '../../../../Resources/Images/Boxes/VanishCapInactive.png';
import iconMetalCapInactive from '../../../../Resources/Images/Boxes/MetalCapInactive.png';
import FlagInput from "./FlagInput";
import type {SaveFlagsType} from "../../../../Data/Save";
import type {CapType} from "../../../../Types/Cap";
import {flags} from "../../../../Data/Flags";

type Props = {
  flags: SaveFlagsType,
  updateSaveData: Function,
};

function capTypeToFlagName(type: CapType) {
  switch (type) {
    case 'wing':
      return flags.unlockedWingCap;
    case 'vanish':
      return flags.unlockedVanishCap;
    case 'metal':
      return flags.unlockedMetalCap;
    default:
      throw new Error(`Unknown type "${type}"`);
  }
}

function capTypeToCurrentValue(saveDataFlags: SaveFlagsType, type: CapType) {
  switch (type) {
    case 'wing':
      return saveDataFlags.unlockedWingCap;
    case 'vanish':
      return saveDataFlags.unlockedVanishCap;
    case 'metal':
      return saveDataFlags.unlockedMetalCap;
    default:
      throw new Error(`Unknown type "${type}"`);
  }
}

export default function Caps(props: Props) {
  const {flags, updateSaveData} = props;

  const onChange = (type: string) => {
    updateSaveData({
      type: 'flag',
      data: {
        flag: capTypeToFlagName(type),
        value: !capTypeToCurrentValue(flags, type),
      }
    });
  }

  return (
    <Row>
      <Col md={1}>
        Caps
      </Col>
      <Col>
        <FlagInput
          type={'wing'}
          nameSuffix={'Cap'}
          image={flags.unlockedWingCap ? iconWingCapActive : iconWingCapInactive}
          onClick={onChange}
        />
        <FlagInput
          type={'vanish'}
          nameSuffix={'Cap'}
          image={flags.unlockedVanishCap ? iconVanishCapActive : iconVanishCapInactive}
          onClick={onChange}
        />
        <FlagInput
          type={'metal'}
          nameSuffix={'Cap'}
          image={flags.unlockedMetalCap ? iconMetalCapActive : iconMetalCapInactive}
          onClick={onChange}
        />
      </Col>
    </Row>
  );
}
