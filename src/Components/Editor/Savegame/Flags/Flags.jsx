// @flow
import React from 'react';
import Caps from "./Caps";
import type {SaveFlagsType} from "../../../../Data/Save";
import Keys from "./Keys";
import {Col, Row} from "react-bootstrap";
import FileInUse from "./FileInUse";
import Doors from "./Doors";

type Props = {
  flags: SaveFlagsType,
  updateSaveData: Function,
};

export default function Flags(props: Props) {
  const {flags, updateSaveData} = props;

  return (
    <div className={'mt-2'}>
      <Col>
        <Row className={'mt-3'}>
          <FileInUse flags={flags} updateSaveData={updateSaveData}/>
        </Row>
        <Row className={'mt-3'}>
          <Caps flags={flags} updateSaveData={updateSaveData}/>
        </Row>
        <Row className={'mt-4'}>
          <Keys flags={flags} updateSaveData={updateSaveData}/>
        </Row>
        <Row className={'mt-4'}>
          <Doors flags={flags} updateSaveData={updateSaveData}/>
        </Row>
      </Col>
    </div>
  );
}
