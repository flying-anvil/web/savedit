// @flow
import React from 'react';
import FlagInput from "./FlagInput";
import iconLockedDoorBasement from "../../../../Resources/Images/Doors/KeyDoorLockedBasement.png";
import iconLockedDoorUpstairs from "../../../../Resources/Images/Doors/KeyDoorLockedUpstairs.png";
import iconLockedDoor from "../../../../Resources/Images/Doors/DoorLocked.png";
import iconStarDoorLocked1 from "../../../../Resources/Images/Doors/StarDoorLocked1.png";
import iconStarDoorLocked2 from "../../../../Resources/Images/Doors/StarDoorLocked2.png";
import iconStarDoorLocked3 from "../../../../Resources/Images/Doors/StarDoorLocked3.png";
import iconUnlockedDoorBasement from "../../../../Resources/Images/Doors/KeyDoorUnlockedBasement.png";
import iconUnlockedDoorUpstairs from "../../../../Resources/Images/Doors/KeyDoorUnlockedUpstaris.png";
import iconUnlockedDoor from "../../../../Resources/Images/Doors/DoorUnlocked.png";
import iconStarDoorUnlocked1 from "../../../../Resources/Images/Doors/StarDoorUnlocked1.png";
import iconStarDoorUnlocked2 from "../../../../Resources/Images/Doors/StarDoorUnlocked2.png";
import iconStarDoorUnlocked3 from "../../../../Resources/Images/Doors/StarDoorUnlocked3.png";
import type {SaveFlagsType} from "../../../../Data/Save";
import type {KeyType} from "../../../../Types/Key";
import {flags} from "../../../../Data/Flags";
import {Col, Row} from "react-bootstrap";

type Props = {
  flags: SaveFlagsType,
  updateSaveData: Function,
};

type UnlockableDoor = 'secret' | 'lockedDoor1' | 'lockedDoor2' | 'lockedDoor3' | 'starDoor1' | 'starDoor2' | 'starDoor3';

function keyTypeToFlagName(type: KeyType|UnlockableDoor) {
  switch (type) {
    case 'basement':
      return flags.keyDoorUnlockedBasement;
    case 'upstairs':
      return flags.keyDoorUnlockedUpstairs;
    case 'secret':
      return flags.starDoorUnlockedSecretSlide;
    case 'lockedDoor1':
      return flags.starDoorUnlockedWhompsFortress;
    case 'lockedDoor2':
      return flags.starDoorUnlockedJollyRogerBay;
    case 'lockedDoor3':
      return flags.starDoorUnlockedCoolCoolMountain;
    case 'starDoor1':
      return flags.starDoorUnlockedBowserDarkWorld;
    case 'starDoor2':
      return flags.starDoorUnlockedBowserFireSea;
    case 'starDoor3':
      return flags.starDoorUnlocked3rdFloor;
    default:
      throw new Error(`Unknown type "${type}"`);
  }
}

function keyTypeToCurrentValue(saveDataFlags: SaveFlagsType, type: KeyType|UnlockableDoor) {
  switch (type) {
    case 'basement':
      return saveDataFlags.keyDoorUnlockedBasement;
    case 'upstairs':
      return saveDataFlags.keyDoorUnlockedUpstairs;
    case 'secret':
      return saveDataFlags.starDoorUnlockedSecretSlide;
    case 'lockedDoor1':
      return saveDataFlags.starDoorUnlockedWhompsFortress;
    case 'lockedDoor2':
      return saveDataFlags.starDoorUnlockedJollyRogerBay;
    case 'lockedDoor3':
      return saveDataFlags.starDoorUnlockedCoolCoolMountain;
    case 'starDoor1':
      return saveDataFlags.starDoorUnlockedBowserDarkWorld;
    case 'starDoor2':
      return saveDataFlags.starDoorUnlockedBowserFireSea;
    case 'starDoor3':
      return saveDataFlags.starDoorUnlocked3rdFloor;
    default:
      throw new Error(`Unknown type "${type}"`);
  }
}

export default function Doors(props: Props) {
  const {flags, updateSaveData} = props;

  const onChange = (type: string) => {
    updateSaveData({
      type: 'flag',
      data: {
        flag: keyTypeToFlagName(type),
        value: !keyTypeToCurrentValue(flags, type),
      }
    });
  }

  return (
    <Row style={{width: '100%'}}>
      <Col md={1}>
        Unlocks
      </Col>
      <Col md={11}>
        <FlagInput
          type={'basement'}
          nameSuffix={'Key Door'}
          image={flags.keyDoorUnlockedBasement ? iconUnlockedDoorBasement : iconLockedDoorBasement}
          width={64}
          onClick={onChange}
        />
        <FlagInput
          type={'upstairs'}
          nameSuffix={'Key Door'}
          image={flags.keyDoorUnlockedUpstairs ? iconUnlockedDoorUpstairs : iconLockedDoorUpstairs}
          width={64}
          onClick={onChange}
        />
        <FlagInput
          type={'secret'}
          title={'Secret Slide'}
          image={flags.starDoorUnlockedSecretSlide ? iconUnlockedDoor : iconLockedDoor}
          onClick={onChange}
        />
        <FlagInput
          type={'lockedDoor1'}
          title={'Locked Door 1'}
          image={flags.starDoorUnlockedWhompsFortress ? iconUnlockedDoor : iconLockedDoor}
          onClick={onChange}
        />
        <FlagInput
          type={'lockedDoor2'}
          title={'Locked Door 2'}
          image={flags.starDoorUnlockedJollyRogerBay ? iconUnlockedDoor : iconLockedDoor}
          onClick={onChange}
        />
        <FlagInput
          type={'lockedDoor3'}
          title={'Locked Door 3'}
          image={flags.starDoorUnlockedCoolCoolMountain ? iconUnlockedDoor : iconLockedDoor}
          onClick={onChange}
        />
        <FlagInput
          type={'starDoor1'}
          title={'Star Door 1'}
          image={flags.starDoorUnlockedBowserDarkWorld ? iconStarDoorUnlocked1 : iconStarDoorLocked1}
          width={64}
          onClick={onChange}
        />
        <FlagInput
          type={'starDoor2'}
          title={'Star Door 2'}
          image={flags.starDoorUnlockedBowserFireSea ? iconStarDoorUnlocked2 : iconStarDoorLocked2}
          width={64}
          onClick={onChange}
        />
        <FlagInput
          type={'starDoor3'}
          title={'Star Door 3'}
          image={flags.starDoorUnlocked3rdFloor ? iconStarDoorUnlocked3 : iconStarDoorLocked3}
          width={64}
          onClick={onChange}
        />
      </Col>
    </Row>
  );
}
