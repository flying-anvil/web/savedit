// @flow
import React from 'react';
import {Col, Row} from "react-bootstrap";
import iconKeyBasementActive from '../../../../Resources/Images/Key/KeyBasement.png';
import iconKeyUpstairsActive from '../../../../Resources/Images/Key/KeyUpstairs.png';
import iconKeyInactive from '../../../../Resources/Images/Key/KeyOutline.png';
import type {SaveFlagsType} from "../../../../Data/Save";
import {flags} from "../../../../Data/Flags";
import type {KeyType} from "../../../../Types/Key";
import FlagInput from "./FlagInput";

type Props = {
  flags: SaveFlagsType,
  updateSaveData: Function,
};

function keyTypeToFlagName(type: KeyType) {
  switch (type) {
    case 'basement':
      return flags.hasKeyBasement;
    case 'upstairs':
      return flags.hasKeyUpstairs;
    default:
      throw new Error(`Unknown type "${type}"`);
  }
}

function keyTypeToCurrentValue(saveDataFlags: SaveFlagsType, type: KeyType) {
  switch (type) {
    case 'basement':
      return saveDataFlags.hasKeyBasement;
    case 'upstairs':
      return saveDataFlags.hasKeyUpstairs;
    case 'unlockedUpstairs':
      return saveDataFlags.keyDoorUnlockedUpstairs;
    case 'unlockedBasement':
      return saveDataFlags.keyDoorUnlockedBasement;
    default:
      throw new Error(`Unknown type "${type}"`);
  }
}

export default function Keys(props: Props) {
  const {flags, updateSaveData} = props;

  const onChange = (type: string) => {
    updateSaveData({
      type: 'flag',
      data: {
        flag: keyTypeToFlagName(type),
        value: !keyTypeToCurrentValue(flags, type),
      }
    });
  }

  return (
    <Row>
      <Col md={1}>
        Keys
      </Col>
      <Col>
        <FlagInput
          type={'basement'}
          nameSuffix={'Key'}
          image={flags.hasKeyBasement ? iconKeyBasementActive : iconKeyInactive}
          onClick={onChange}
        />
        <FlagInput
          type={'upstairs'}
          nameSuffix={'Key'}
          image={flags.hasKeyUpstairs ? iconKeyUpstairsActive : iconKeyInactive}
          onClick={onChange}
        />
      </Col>
    </Row>
  );
}
