// @flow
import React, {useCallback} from 'react';
import type {LevelDataType} from "../../../../Data/Save";
import {Button, Col, Form, InputGroup, Row} from "react-bootstrap";
import iconStarBlue from '../../../../Resources/Images/Star/star-blue.png';
import iconStarYellow from '../../../../Resources/Images/Star/star-yellow.png';
import iconCoin from '../../../../Resources/Images/segment2.05800.rgba16.png';
import iconCannonActive from '../../../../Resources/Images/Cannon.png';
import iconCannonInactive from '../../../../Resources/Images/CannonOutline.png';
import FlagInput from "../Flags/FlagInput";
import {course} from "../../../../Data/Course";
import {translateCourse, translateStar} from "../../../../Data/Translation/Translation";

type Props = {
  identifier: string,
  levelData: LevelDataType,
  highscore: ?number,
  updateSaveData: Function,
};

export default function SingleLevelData(props: Props) {
  const {identifier, levelData, highscore, updateSaveData} = props;

  const onStarChange = (type: string, suffix: string) => {
    const star = `${type}${suffix}`;
    updateSaveData({
      type: 'star',
      data: {
        level: identifier,
        star: star,
        value: !levelData.stars[star],
      }
    });
  }

  const onMultiStarChange = useCallback((value: boolean) => {
    updateSaveData({
      type: 'multiStar',
      data: {
        level: identifier,
        stars: {
          star1: value,
          star2: value,
          star3: value,
          star4: value,
          star5: value,
          star6: value,
          starBonus: value,
        },
      }
    });
  }, [identifier, updateSaveData]);

  const onCannonChange = () => {
    updateSaveData({
      type: 'cannon',
      data: {
        level: identifier,
        value: !levelData.cannonUnlocked,
      }
    });
  }

  const onHighscoreChange = (event) => {
    let newValue = parseInt(event.target.value);

    if (isNaN(newValue)) {
      newValue = 0;
    }

    updateSaveData({
      type: 'highscore',
      data: {
        level: identifier,
        value: newValue,
      }
    });
  }

  return (
    <Row className={'mt-2'}>
      <Col md={4} className={'align-middle'}>{translateCourse(identifier)}</Col>
      <Col md={6}>
        {[1, 2, 3, 4, 5, 6, 'Bonus'].map((suffix) =>
          <FlagInput
            type={'star'}
            nameSuffix={suffix}
            image={levelData.stars[`star${suffix}`] ? iconStarYellow : iconStarBlue}
            title={translateStar(identifier, `star${suffix}`)}
            className={suffix === 'Bonus' ? 'ml-2' : null}
            onClick={onStarChange}
            key={suffix}
          />
        )}
        <Button className={'ml-3'} size={'sm'} variant={"outline-warning"} onClick={() => onMultiStarChange(true)}>All</Button>
        <Button className={'ml-2'} size={'sm'} variant={"outline-primary"} onClick={() => onMultiStarChange(false)}>None</Button>
        {
          identifier !== course.castle && identifier !== course.endCake &&
          <FlagInput
            type={'cannon'}
            nameSuffix={''}
            image={levelData.cannonUnlocked ? iconCannonActive : iconCannonInactive}
            className={'ml-2'}
            onClick={onCannonChange}
          />
        }
      </Col>
      <Col md={2}>{
        highscore !== undefined &&
        <InputGroup>
          <InputGroup.Prepend>
            <InputGroup.Text>
              <img src={iconCoin} alt={'Highscore'} className={'nearest-neighbor'}/>
            </InputGroup.Text>
          </InputGroup.Prepend>
          <Form.Control
            id={`highscore-${identifier}`}
            type="number"
            placeholder="Highscore"
            min={0}
            max={255}
            value={highscore}
            onChange={onHighscoreChange}
          />
        </InputGroup>
      }</Col>
    </Row>
  );
}
