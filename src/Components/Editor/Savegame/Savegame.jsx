// @flow
import React from 'react';
import {Tab, Tabs} from "react-bootstrap";
import Flags from "./Flags/Flags";
import type {SaveSlotKeyType, SaveSlotType} from "../../../Data/Save";
import {FaFlag} from "react-icons/all";
import LevelData from "./LevelData/LevelData";

type Props = {
  slot: SaveSlotKeyType,
  slotData: SaveSlotType,
  updateSaveData: Function,
};

export default function Savegame(props: Props) {
  const {slot, slotData, updateSaveData} = props;

  const doUpdate = (payload) => {
    payload.data.slot = slot;
    updateSaveData(payload);
  }

  return (
    <div className={'mt-3'}>
      <Tabs defaultActiveKey={'level-data'} id={`savegame-section-${slot}`}>
        <Tab title={'Level Data'} eventKey={'level-data'}>
          <LevelData levelData={slotData.levelData} highscores={slotData.highscores} updateSaveData={doUpdate}/>
        </Tab>
        <Tab title={<>Flags <FaFlag/></>} eventKey={'flags'}>
          <Flags flags={slotData.flags} updateSaveData={doUpdate}/>
        </Tab>
      </Tabs>
    </div>
  );
}
