// @flow
import React from 'react';
import SoundSettings from "./SoundSettings";
import LanguageSettings from "./LanguageSettings";
import type {GlobalDataType} from "../../../Data/Save";

type Props = {
  globalData: GlobalDataType,
  updateSaveData: Function,
};

export default function Settings(props: Props) {
  const {globalData, updateSaveData} = props;

  const onUpdate = (setting: 'sound' | 'language', newValue: number) => {
    updateSaveData({
      type: 'global',
      data: {
        setting: setting,
        value: newValue,
      }
    });
  }

  return (
    <div className={'mt-3'}>
      <SoundSettings value={globalData.sound} update={(newValue) => onUpdate('sound', newValue)}/>
      <LanguageSettings value={globalData.language} update={(newValue) => onUpdate('language', newValue)}/>
    </div>
  );
}
