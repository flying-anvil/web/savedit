// @flow
import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Navigation from "./Navigation/Navigation";
import {Col} from "react-bootstrap";
import Editor from "./Editor/Editor";
import EditorStyleGame from "./EditorStyleGame/EditorStyleGame";

type Props = {};

export default function Router(props: Props) {
  return (
    <BrowserRouter>
      <Navigation/>

      <Col md={12}>
        <Switch>

          <Route exact path={'/'}>
            <Editor/>
          </Route>
          <Route exact path={'/default'}>
            <Editor/>
          </Route>
          <Route exact path={'/game'}>
            <EditorStyleGame/>
          </Route>

        </Switch>
      </Col>
    </BrowserRouter>
  );
};
