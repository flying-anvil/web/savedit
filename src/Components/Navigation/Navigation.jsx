// @flow
import React from 'react';
import {Nav, Navbar} from "react-bootstrap";
import {Link} from "react-router-dom";
import NavItem from "./NavItem";

type Props = {};

export default function Navigation(props: Props) {
  return (
    <>
      <Navbar>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Link to={'/'} className="navbar-brand">SM64 Savedit</Link>

          <Nav className="mr-4">
            <NavItem as={Link} to={'/default'} text={'Default'}/>
          </Nav>

          <Nav className="mr-4">
            <NavItem as={Link} to={'/game'} text={'Game Style'}/>
          </Nav>

        </Navbar.Collapse>
      </Navbar>
    </>
  );
};
