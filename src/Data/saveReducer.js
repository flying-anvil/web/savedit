// @flow

import type {GlobalDataType, SaveSlotType, SaveType} from "./Save";
import {SaveFlagsType} from "./Save";
import produce from "immer";

type ActionType = {
  section: 'global' | 'flag',
  data: Object,
};

type GlobalActionType = {
  setting: 'sound' | 'language',
  value: number,
}

type SaveGameActionType = {
  type: 'flag',
  data: Object,
}

type FlagActionType = {
  type: 'flag',
  value: boolean,
}

function reduceGlobal(state: GlobalDataType, action: GlobalActionType) {
  const {setting, value} = action;

  return {...state, [setting]: value};
}

function reduceSaveData(state: SaveSlotType, action: SaveGameActionType) {
  const {type, data} = action;

  switch (type) {
    case 'flag':
      return {...state, flags: reduceFlags(state.flags, data)};
    default:
      throw new Error(`Unknown type "${type}"`);
  }
}

function reduceFlags(state: SaveFlagsType, action: FlagActionType) {
  return {...state, [action.type]: action.value}
}

function applyFlag(state: SaveType, data: FlagActionType) {
  return produce(state, (draft) => {

  });
}

export default function saveReducer(state: SaveType, action: ActionType) {
  const {section, data} = action;

  switch (section) {
    case 'global':
      return {...state, globalData: reduceGlobal(state.globalData, data)};
    case 'flag':

    // case 'save':
    //   return {...state, saveData: reduceSaveData(state.saveData, data)};
    default:
      throw new Error(`Unknown section "${section}"`);
  }
}
