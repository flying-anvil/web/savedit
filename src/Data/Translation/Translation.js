// @flow

import type {CourseIdentifier} from '../Course'

import {vanillaTranslation} from "./Vanilla";
import {templateTranslation} from "./Template";
import {template2Translation} from "./Template2";

export type TranslationTargetType = 'vanilla';

type StarsTranslationType = {
  [CourseIdentifier]: {
    star1: string,
    star2: string,
    star3: string,
    star4: string,
    star5: string,
    star6: string,
    starBonus: string,
  }
}

type CourseTranslationType = {
  [CourseIdentifier]: string
}

type TranslationType = {
  stars: StarsTranslationType,
  levels: CourseTranslationType,
}

type TranslationsType = {
  [string]: {
    [string]: TranslationType,
  }
}

export const translations: TranslationsType = {
  template: {
    english: templateTranslation,
  },
  template2: {
    english: template2Translation,
  },
  vanilla: {
    english: vanillaTranslation,
  },
}

export function translateCourse(course: string, translationTarget: TranslationTargetType = 'vanilla', language: string = 'english'): string {
  if (!translations.hasOwnProperty(translationTarget)) {
    return `Missing Translation Target (${translationTarget})`;
  }

  const target = translations[translationTarget];

  if (!target.hasOwnProperty(language)) {
    return `Missing Translation Language (${translationTarget}.${language})`;
  }

  const translation = target[language];

  if (!translation.levels.hasOwnProperty(course)) {
    return `Missing Translation Course (${translationTarget}.${language}.levels.${course})`;
  }

  return translations[translationTarget][language].levels[course];
}

export function translateStar(course: CourseIdentifier, star: string, translationTarget: TranslationTargetType = 'vanilla', language: string = 'english'): string {
  if (!translations.hasOwnProperty(translationTarget)) {
    return `Missing Translation Target (${translationTarget})`;
  }

  const target = translations[translationTarget];

  if (!target.hasOwnProperty(language)) {
    return `Missing Translation Language (${translationTarget}.${language})`;
  }

  const translation = target[language];

  if (!translation.stars.hasOwnProperty(course)) {
    return `Missing Translation Star Course (${translationTarget}.${language}.stars.${course})`;
  }

  const allStars = translation.stars[course];

  if (!allStars.hasOwnProperty(star)) {
    return `Missing Translation Star (${translationTarget}.${language}.stars.${course}.${star})`;
  }

  return translations[translationTarget][language].stars[course][star];
}

